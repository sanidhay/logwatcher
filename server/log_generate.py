import logging
import random, time

LOG_FILE = './test.log'
open('./test.log', 'w').close()
logging.basicConfig(filename = LOG_FILE, level = logging.DEBUG)
for i in range(1000):
    time.sleep(random.randint(1,4))
    msg = "this is log message " + str(i)
    logging.debug(msg)
