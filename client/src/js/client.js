
App = {
    
    init: function() {
        console.log("App initialised");
        let ajax = new XMLHttpRequest();
        let previous = ""
        ajax.onreadystatechange = () => {
            if (ajax.readyState == 4) {
                if (ajax.responseText != previous) {
                    // alert("file changed!");
                    previous = ajax.responseText;
                    console.log("check ", ajax.responseText);
                    log_lines = previous.split("\n");
                    $("#logs").html("");
                    curr = "";
                    for(let i = Math.max(log_lines.length - 11, 0); i < log_lines.length; i++) {
                        curr = log_lines[i];
                        $("#logs").append("<b>" + curr + "</b><br />");
                    }
                }
            }
        };
        ajax.open("GET", "./test.log", true); 
        ajax.send();
        // return App.log();
    },

    log: function() {
        log_file = "test.log"
        var log_previous = "";

        var lajax = new XMLHttpRequest();
        lajax.onreadystatechange = () => {
            if (lajax.readyState == 4) {
                if (lajax.responseText != log_previous) {
                    // alert("file changed!");
                    log_previous = lajax.responseText;
                    console.log("check ", lajax.responseText);
                    log_lines = log_previous.split("\n");
                    // $("#llogs").html("");
                    console.log("log lines ", log_lines.length)
                    curr = log_lines[log_lines.length - 2];
                    $("#llogs").append("<b>" + curr + "</b><br />");
                }
            }
        };
        lajax.open("GET", "./test.log", true); 
        lajax.send();
    }
}

$(() => {
    $(window).load(() => {
      App.init();
      setInterval(() => {
          App.log();
      }, 1000)
    })
});
