# Log Watching Solution

## Installation and Run

 - go to "client" directory and run `npm i`
 - npm v12.9.1
 - run `npm run start`
 - go to "server" directory and run `python3 log_generate.py` in another terminal window
 - go to `localhost:8000` in your browser tab
